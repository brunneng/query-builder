package org.bitbucket.brunneng.qb;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.ResultTransformer;

/**
 * HQL Query builder which uses hibernate {@link Session} to build queries.
 */
public class HibernateQueryBuilder extends AbstractJpqlQueryBuilder {

  private final Session session;

  public HibernateQueryBuilder(Session session) {
    this.session = session;
  }

  public HibernateQueryBuilder(EntityManager entityManager) {
    this(entityManager.unwrap(Session.class));
  }

  /**
   * Builds hql query with applied parameters
   *
   * @return hql query ready to be executed
   */
  public Query build() {
    return (Query) super.build();
  }

  /**
   * Builds typed hql query with applied parameters
   *
   * @param resultClass expected type of entity
   * @return typed hql query ready to be executed
   */
  public <T> Query<T> build(Class<T> resultClass) {
    return (Query<T>) super.build(resultClass);
  }

  /**
   * Builds native query with applied parameters
   *
   * @return native query ready to be executed
   */
  public NativeQuery buildNative() {
    return (NativeQuery) super.buildNative();
  }

  /**
   * Build native query with applying given transformer to transform query results to dto
   *
   * @param resultTransformer the result transformer with should be used
   * @return query with applied transformer
   * @see ResultTransformer
   */
  public <T> NativeQuery<T> buildNative(ResultTransformer resultTransformer) {
    NativeQuery res = (NativeQuery) super.buildNative();
    res.setResultTransformer(resultTransformer);
    return res;
  }

  /**
   * Build native query with applying hibernate AliasToBeanResultTransformer to transform query
   * results to dto
   *
   * @param resultDto expected type of result dto
   * @return query with applied transformer
   * @see AliasToBeanResultTransformer
   */
  public <T> NativeQuery<T> buildNativeWithAliasToBeanResultTransformer(Class<T> resultDto) {
    return buildNative(new AliasToBeanResultTransformer(resultDto));
  }

  /**
   * Builds hql query to get a count of objects, selected by current query. For example if your
   * query is <i>`select o from Order o where ...`</i>, then built query will be
   * <i>`select count(o) from Order o where ...`</i>.
   *
   * @return hql count query, to be executed
   */
  public Query<Long> buildCountQuery() {
    return (Query<Long>) super.buildCountQuery();
  }

  /**
   * Builds native query to get a count of objects, selected by current query. For example if your
   * query is <i>`select o.id from orders o where ...`</i>, then built query will be
   * <i>`select count(o.id) from orders o where ...`</i>.
   *
   * @return native count query, to be executed
   */
  public NativeQuery<Number> buildCountNativeQuery() {
    return (NativeQuery<Number>) super.buildCountNativeQuery();
  }

  @Override
  protected jakarta.persistence.Query createQuery(QueryWithParams queryWithParams) {
    var query = session.createQuery(queryWithParams.getQuery());
    applyParametersToQuery(query, queryWithParams.getParameters());

    return query;
  }

  @Override
  protected <T> TypedQuery<T> createTypedQuery(QueryWithParams queryWithParams,
      Class<T> targetClass) {
    TypedQuery<T> query = session.createQuery(queryWithParams.getQuery(), targetClass);
    applyParametersToQuery(query, queryWithParams.getParameters());
    return query;
  }

  @Override
  protected jakarta.persistence.Query createNativeQuery(QueryWithParams queryWithParams) {
    var query = session.createNativeQuery(queryWithParams.getQuery());
    applyParametersToQuery(query, queryWithParams.getParameters());
    return query;
  }
}
