package org.bitbucket.brunneng.qb;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;

/**
 * JPQL Query builder which uses jpa {@link EntityManager} to build queries.
 */
public class JpaQueryBuilder extends AbstractJpqlQueryBuilder {

  private final EntityManager entityManager;

  public JpaQueryBuilder(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  @Override
  protected Query createQuery(QueryWithParams queryWithParams) {
    Query query = entityManager.createQuery(queryWithParams.getQuery());
    applyParametersToQuery(query, queryWithParams.getParameters());
    return query;
  }

  @Override
  protected <T> TypedQuery<T> createTypedQuery(QueryWithParams queryWithParams,
      Class<T> targetClass) {
    TypedQuery<T> query = entityManager.createQuery(queryWithParams.getQuery(), targetClass);
    applyParametersToQuery(query, queryWithParams.getParameters());
    return query;
  }

  @Override
  protected Query createNativeQuery(QueryWithParams queryWithParams) {
    Query query = entityManager.createNativeQuery(queryWithParams.getQuery());
    applyParametersToQuery(query, queryWithParams.getParameters());
    return query;
  }

}
