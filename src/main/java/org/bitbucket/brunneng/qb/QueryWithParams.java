package org.bitbucket.brunneng.qb;

import java.util.Collections;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Immutable POJO which holds query string and it's named parameters map.
 */
public class QueryWithParams {
   private final String query;
   private final Map<String, Object> parameters;

   public QueryWithParams(String query, Map<String, Object> parameters) {
      this.query = query;
      this.parameters = Collections.unmodifiableMap(parameters);
   }

   public String getQuery() {
      return query;
   }

   public Map<String, Object> getParameters() {
      return parameters;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", "[", "]")
            .add("query='" + query + "'")
            .add("parameters=" + parameters)
            .toString();
   }
}
