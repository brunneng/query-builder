package org.bitbucket.brunneng.qb

import java.time.LocalDate

class SqlQueryBuilderTest<QB extends SqlQueryBuilder> extends QueryBuilderTest<QB> {

  def 'test appendLike validation failed'(String queryPart, String value) {
    when:
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLike(queryPart, value)
    then:
    thrown IllegalArgumentException
    where:
    queryPart | value
    "o.name lika :v" | "a"
    "o.name lika :v" | null
    "like :v" | "a"
    "o.name in :v like" | "a"
  }

  def 'test appendLikeStartsWith validation failed'(String queryPart, String value) {
    when:
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeStartsWith(queryPart, value)
    then:
    thrown IllegalArgumentException
    where:
    queryPart | value
    "o.name lika :v" | "a"
    "o.name lika :v" | null
    "like :v" | "a"
    "o.name in :v like" | "a"
  }

  def 'test appendLikeEndsWith validation failed'(String queryPart, String value) {
    when:
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeEndsWith(queryPart, value)
    then:
    thrown IllegalArgumentException
    where:
    queryPart | value
    "o.name lika :v" | "a"
    "o.name lika :v" | null
    "like :v" | "a"
    "o.name in :v like" | "a"
  }

  def 'test appendLikeCaseInsensitive validation failed'(String queryPart, String value) {
    when:
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeCaseInsensitive(queryPart, value)
    then:
    thrown IllegalArgumentException
    where:
    queryPart | value
    "o.name lika :v" | "a"
    "o.name lika :v" | null
    "like :v" | "a"
    "o.name in :v like" | "a"
  }

  def 'test appendIn validation failed'(String queryPart, List<String> value) {
    when:
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIn(queryPart, value)
    then:
    thrown IllegalArgumentException
    where:
    queryPart | value
    "o.name im :v" | ["a"]
    "o.name im :v" | []
    "o.name im :v" | null
    "o.name like :v in" | ["a"]
    "in o.name like :v" | ["a"]
  }

  def 'test buildCountQuery illegal'(String query) {
    when:
    def qb = createQueryBuilder()
    qb.append(query)
    qb.buildCountQueryInternal(true)
    then:
    thrown IllegalStateException
    where:
    query                 | _
    "select o ff Order o" | _
    "ss o from Order o"   | _
  }

  def 'test buildCountNativeQuery illegal'(String query) {
    when:
    def qb = createQueryBuilder()
    qb.append(query)
    qb.buildCountQueryInternal(false)
    then:
    thrown IllegalStateException
    where:
    query                     | _
    "select o.id ff orders o" | _
    "ss o.id from orders o"   | _
    "from orders o"           | _
  }

  def 'test findTargetAlias failed'(String query) {
    when:
    def qb = createQueryBuilder()
    qb.append(query)
    qb.findTargetAlias()
    then:
    thrown IllegalStateException
    where:
    query           | _
    "fro Order o"   | _
    "fromOrder o"   | _
    "from Ordero"   | _
    "from 1Order o" | _
    "from Order 1o" | _
  }

  def 'test remove first "and"'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from order where")
    qb.append("and id=:v", 5)
    qb.append("and total>:v", 100)
    then:
    qb.getQuery().toString() == "select * from order where id=:id and total>:total"
  }

  def 'test remove first "and", spaces at start'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from order where")
    qb.append("   And id=5")
    then:
    qb.getQuery().toString() == "select * from order where id=5"
  }

  def 'test remove "where" without conditions'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from order where")
    qb.append("and id=:v", null)
    then:
    qb.getQuery().toString() == "select * from order"
  }

  def 'test remove "where" without conditions, spaces in the end'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from order wHerE   ")
    qb.append("and id=:v", null)
    then:
    qb.getQuery().toString() == "select * from order   "
  }

  def 'test remove "where" without conditions with "order by"'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from person where")
    qb.append("and id=:v", null)
    qb.append("order by name")
    then:
    qb.getQuery().toString() == "select * from person order by name"
  }

  def 'test remove "where" without conditions with "group by"'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from person where")
    qb.append("and id=:v", null)
    qb.append("group by name")
    then:
    qb.getQuery().toString() == "select * from person group by name"
  }

  def 'test remove "where" without conditions with sub-query'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select *, (select a.town from address where) from person where")
    qb.append("and id=:v", null)
    qb.append("order by name")
    then:
    qb.getQuery().toString() == "select *, (select a.town from address) from person order by name"
  }

  def 'test append interval intersection'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from relation r where")
    qb.appendIntervalIntersection("r.fromDate", "r.toDate",
            LocalDate.of(2020, 5, 1),
            LocalDate.of(2020, 6, 1), true)
    then:
    qb.getQuery().toString() == "select * from relation r where " +
            "((r.fromDate is null or r.fromDate <= :rFromDate) and (r.toDate is null or r.toDate >= :rToDate))"
  }

  def 'test append interval intersection, start is null'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from relation r where")
    qb.appendIntervalIntersection("r.fromDate", "r.toDate",
            null,
            LocalDate.of(2020, 6, 1), true)
    then:
    qb.getQuery().toString() == "select * from relation r where " +
            "(r.fromDate is null or r.fromDate <= :rFromDate)"
  }

  def 'test append interval intersection, end is null'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from relation r where")
    qb.appendIntervalIntersection("r.fromDate", "r.toDate",
            LocalDate.of(2020, 5, 1),
            null, true)
    then:
    qb.getQuery().toString() == "select * from relation r where " +
            "(r.toDate is null or r.toDate >= :rToDate)"
  }

  def 'test append interval intersection, start and end are null'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from relation r where")
    qb.appendIntervalIntersection("r.fromDate", "r.toDate",
            null,
            null, true)
    then:
    qb.getQuery().toString() == "select * from relation r"
  }

  def 'test append interval intersection, without including end'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select * from relation r where")
    qb.appendIntervalIntersection("r.fromDate", "r.toDate",
            LocalDate.of(2020, 5, 1),
            LocalDate.of(2020, 6, 1), false)
    then:
    qb.getQuery().toString() == "select * from relation r where " +
            "((r.fromDate is null or r.fromDate < :rFromDate) and (r.toDate is null or r.toDate > :rToDate))"
  }

  protected SqlQueryBuilder createQueryBuilder() {
    return new SqlQueryBuilder()
  }

}
