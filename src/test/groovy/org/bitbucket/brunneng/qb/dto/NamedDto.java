package org.bitbucket.brunneng.qb.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class NamedDto {

  private Long id;
  private String name;

  void setId(Number id) {
    this.id = id.longValue();
  }
}
