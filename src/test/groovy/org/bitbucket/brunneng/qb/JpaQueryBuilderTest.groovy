package org.bitbucket.brunneng.qb

class JpaQueryBuilderTest extends AbstractJpqlQueryBuilderTest<JpaQueryBuilder> {

  protected JpaQueryBuilder createQueryBuilder() {
    return new JpaQueryBuilder(entityManager)
  }
}
