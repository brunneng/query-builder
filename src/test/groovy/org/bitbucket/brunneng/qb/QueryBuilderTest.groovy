package org.bitbucket.brunneng.qb

import jakarta.transaction.Transactional
import spock.lang.Specification

@Transactional
class QueryBuilderTest<QB extends QueryBuilder> extends Specification {

  def 'test constructParameterValueName'(String appendPart, String expectedName) {
    when:
    QueryBuilder qb = createQueryBuilder()
    qb.registerOperator("like")
    qb.registerOperator("not")
    then:
    qb.constructFullNameOfShortParameter(appendPart, 0) == expectedName
    where:
    appendPart                        | expectedName
    " and p.name = :v"                | "pName"
    " and p.address.street = :v"      | "pAddressStreet"
    " and p.address.street like :v"   | "pAddressStreet"
    " and p.name LiKe :v"             | "pName"
    " and p.name not like :v"         | "pName"
    "p.name = :v"                     | "pName"
    ".name = :v"                      | "name"
    "name = :v"                       | "name"
    "name  =  :v "                    | "name"
    "name=:v"                         | "name"
    "name:v"                          | "name"
    " = :v"                           | null
    " (!**182 = :v"                   | "v182"
    " and p.name = :value"            | null
    " and p.name = :va"               | null
    " and p.name = :v and p.age = :v" | "pName"
    " and p1_2.name = :v"             | "p1_2Name"
  }

  def 'test incrementName'(String name, String newName) {
    expect:
    QueryBuilder.incrementName(name) == newName
    where:
    name     | newName
    "name"   | "name2"
    "name2"  | "name3"
    "na2me2" | "na2me3"
    "name9"  | "name10"
  }

  def 'test append'() {
    when:
    def qb = createQueryBuilder()
    qb.append("p.name = :v", "a");
    qb.append(" or p.name = :v", "b");
    qb.append(" or p.age > :v", null);
    qb.append(" or p.name2 = :pName2", "c");
    def parameters = qb.getParameters()
    then:
    qb.getQuery() == "p.name = :pName or p.name = :pName2 or p.name2 = :pName3"
    parameters["pName"] == "a"
    parameters["pName2"] == "b"
    parameters["pName3"] == "c"
  }

  def 'test append more then one parameters at once'() {
    when:
    def qb = createQueryBuilder()
    qb.append("p.firstName = :v or p.secondName = :v", "Jo");
    def parameters = qb.getParameters()
    then:
    qb.getQuery() == "p.firstName = :pFirstName or p.secondName = :pSecondName"
    parameters["pFirstName"] == "Jo"
    parameters["pSecondName"] == "Jo"
  }

  def 'test auto add space'() {
    when:
    def qb = createQueryBuilder()
    qb.append("part1");
    qb.append("part2");
    qb.append(" part3");
    qb.append("or p.name = :v", "b");
    then:
    qb.getQuery() == "part1 part2 part3 or p.name = :pName"
  }

  protected QB createQueryBuilder() {
    return new QueryBuilder()
  }
}
