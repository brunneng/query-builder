package org.bitbucket.brunneng.qb

import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import org.bitbucket.brunneng.qb.model.Order
import org.bitbucket.brunneng.qb.repo.OrderRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql

import java.time.LocalDate

@SpringBootTest(classes = TestApplication.class)
@Sql(statements = "delete from orders", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
abstract class AbstractJpqlQueryBuilderTest<QB extends AbstractJpqlQueryBuilder> extends SqlQueryBuilderTest<QB> {

  @PersistenceContext
  protected EntityManager entityManager

  @Autowired
  protected OrderRepository orderRepository

  def 'test appendLike'() {
    when:
    def o1 = createOrder("abc1")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLike("o.name like :v", "bc")
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test append equal - no spaces around ='() {
    when:
    def o1 = createOrder("ab")
    createOrder("abc")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.append("o.name=:v", "ab")
    qb.append("and o=o")
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test append two conditions at once'() {
    when:
    def o1 = createOrder("ab")
    createOrder("abc")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.append("o.name = :v and o.name like :v", "ab")
    qb.append("and o=o")
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendLike native'() {
    when:
    def o1 = createOrder("abc1")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o.name from orders o where")
    qb.appendLike("o.name like :v", "bc")
    def query = qb.buildNative()
    then:
    query.resultList == [o1.name]
  }

  def 'test appendLikeStartsWith'() {
    when:
    def o1 = createOrder("abc1")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeStartsWith("o.name like :v", "ab")
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]

    when:
    qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeStartsWith("o.name like :v", "bc")
    query = qb.build(Order.class)
    then:
    query.resultList == []
  }

  def 'test appendLikeEndsWith'() {
    when:
    def o1 = createOrder("abc1")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeEndsWith("o.name like :v", "c1")
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]

    when:
    qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeEndsWith("o.name like :v", "bc")
    query = qb.build(Order.class)
    then:
    query.resultList == []
  }

  def 'test appendLikeCaseInsensitive'() {
    when:
    def o1 = createOrder("abC1", "DeSC")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeCaseInsensitive("o.name like :v", "Bc")
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]

    when:
    qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeCaseInsensitive("o.name LiKe :v", "Bc")
    query = qb.build(Order.class)
    then:
    query.resultList == [o1]

    when:
    qb = createQueryBuilder()
    qb.append("select o from Order o where 1=1")
    qb.appendLikeCaseInsensitive("and (o.name LiKe :v)", "Bc")
    query = qb.build(Order.class)
    then:
    query.resultList == [o1]

    when:
    qb = createQueryBuilder()
    qb.append("select o from Order o where 1=1")
    qb.appendLikeCaseInsensitive("and (o.name like :v or o.description like :v)", "dEs")
    query = qb.build(Order.class)
    then:
    query.resultList == [o1]

    when:
    qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendLikeCaseInsensitive("o.namelike :v", "Bc")
    query = qb.build(Order.class)
    then:
    thrown IllegalArgumentException
  }

  def 'test appendIn'() {
    when:
    def o1 = createOrder("abc1")
    def o2 = createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("from Order o where")
    qb.appendIn("o.name in :v", ["abc1", "cde2"])
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]

    when:
    qb = createQueryBuilder()
    qb.append("select o_1 from Order o_1 where 1=1")
    qb.appendIn("and o_1.name in :o_1Name", [])
    query = qb.build(Order.class)
    then:
    query.resultList == [o1, o2]
  }

  def 'test buildCountQuery'(String query) {
    when:
    createOrder("abc1")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append(query)
    def count = qb.buildCountQuery().singleResult
    then:
    count instanceof Long
    count == 2
    where:
    query                      | _
    "select o from Order o"    | _
    "select  o  from  Order o" | _
    "sELect o FroM Order o"    | _
    "from Order o"             | _
    "  from  Order o"          | _
    "From Order o"             | _
    "From Order o"             | _
  }

  def 'test buildCountQuery illegal'(String query) {
    when:
    def qb = createQueryBuilder()
    qb.append(query)
    qb.buildCountQuery()
    then:
    thrown IllegalStateException
    where:
    query                 | _
    "select o ff Order o" | _
    "ss o from Order o"   | _
  }

  def 'test buildCountNativeQuery'(String nativeQuery) {
    when:
    createOrder("abc1")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append(nativeQuery)
    def count = qb.buildCountNativeQuery().singleResult
    then:
    count.longValue() == 2
    where:
    nativeQuery                    | _
    "select o.id from orders o"    | _
    "select  o.id  from  orders o" | _
    "sELect o.id FroM orders o"    | _
    "select * from orders o"       | _
    "select o.* from orders o"     | _
  }

  def 'test buildCountNativeQuery illegal'(String query) {
    when:
    def qb = createQueryBuilder()
    qb.append(query)
    qb.buildCountNativeQuery()
    then:
    thrown IllegalStateException
    where:
    query                     | _
    "select o.id ff orders o" | _
    "ss o.id from orders o"   | _
    "from orders o"           | _
  }

  def 'test findTargetAlias'(String query, String alias) {
    when:
    def qb = createQueryBuilder()
    qb.append(query)
    then:
    qb.findTargetAlias() == alias
    where:
    query                            | alias
    "select o from Order o"          | "o"
    "select  o   from  Order   o"    | "o"
    "select o.id from Order o"       | "o"
    "select o1_a.id from Order o1_a" | "o1_a"
    "from Order o"                   | "o"
  }

  def 'test append join - inserted'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1")
    qb.append("and a.town = :v", "London")
    then:
    qb.getQuery() == "select p from Person p join p.address a where 1=1 and a.town = :aTown"
  }

  def 'test append join - inserted, alias with numbers'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a1")
    qb.append("where 1=1")
    qb.append("and a1.town = :v", "London")
    then:
    qb.getQuery() == "select p from Person p join p.address a1 where 1=1 and a1.town = :a1Town"
  }

  def 'test append join - inserted, start of line'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1 and")
    qb.append("a.town = :v", "London")
    then:
    qb.getQuery() == "select p from Person p join p.address a where 1=1 and a.town = :aTown"
  }

  def 'test append join - inserted by alias without property'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1")
    qb.append("and a = :v", "London")
    then:
    qb.getQuery() == "select p from Person p join p.address a where 1=1 and a = :a"
  }

  def 'test append join - inserted by alias without property, no spaces'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1")
    qb.append("and a=:v", "London")
    then:
    qb.getQuery() == "select p from Person p join p.address a where 1=1 and a=:a"
  }

  def 'test append join - inserted by alias without property, after ='() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1")
    qb.append("and x=a ")
    then:
    qb.getQuery() == "select p from Person p join p.address a where 1=1 and x=a "
  }

  def 'test append join - inserted by alias without property, after =, end of line'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1")
    qb.append("and x=a")
    then:
    qb.getQuery() == "select p from Person p join p.address a where 1=1 and x=a"
  }

  def 'test append join - inserted by alias without property, before =, start of line'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1 and")
    qb.append("a=x")
    then:
    qb.getQuery() == "select p from Person p join p.address a where 1=1 and a=x"
  }

  def 'test append join - not inserted'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1")
    qb.append("and p.name = :v", "John")
    then:
    qb.getQuery() == "select p from Person p where 1=1 and p.name = :pName"
  }

  def 'test append join - not inserted, null parameter'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select p from Person p")
    qb.appendJoin("join p.address a")
    qb.append("where 1=1")
    qb.append("and a.town = :v", null)
    then:
    qb.getQuery() == "select p from Person p where 1=1"
  }

  def 'test append join - inserted immediately by alias with property'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select a.town from Person p")
    qb.appendJoin("join p.address a")
    then:
    qb.getQuery() == "select a.town from Person p join p.address a"
  }

  def 'test append join - inserted immediately by alias without property'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select a from Person p")
    qb.appendJoin("join p.address a")
    then:
    qb.getQuery() == "select a from Person p join p.address a"
  }

  def 'test append join - inserted by chain'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select s from Subscription s")
    qb.appendJoin("join s.customer c")
    qb.appendJoin("join c.person p")
    qb.append("where 1=1")
    qb.append("and p.name = :v", "John")
    then:
    qb.getQuery() == "select s from Subscription s join s.customer c join c.person p where 1=1 and p.name = :pName"
  }

  def 'test append join - not inserted by chain, only one join inserted'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select s from Subscription s")
    qb.appendJoin("join s.customer c")
    qb.appendJoin("join c.person p")
    qb.append("where 1=1")
    qb.append("and c.email = :v", "test@gmail.com")
    qb.append("and p.name = :v", null)
    then:
    qb.getQuery() == "select s from Subscription s join s.customer c where 1=1 and c.email = :cEmail"
  }

  def 'test append join - inserted few joins, param 1'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select s from Subscription s")
    qb.appendJoin("join s.customer c join c.person p")
    qb.append("where 1=1")
    qb.append("and c.email = :v", "test@gmail.com")
    qb.append("and p.name = :v", null)
    then:
    qb.getQuery() == "select s from Subscription s join s.customer c join c.person p where 1=1 and c.email = :cEmail"
  }

  def 'test append join - inserted few joins, param 2'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select s from Subscription s")
    qb.appendJoin("join s.customer c join c.person p")
    qb.append("where 1=1")
    qb.append("and c.email = :v", null)
    qb.append("and p.name = :v", "Joe")
    then:
    qb.getQuery() == "select s from Subscription s join s.customer c join c.person p where 1=1 and p.name = :pName"
  }

  def 'test append join - not inserted few joins, null params'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select s from Subscription s")
    qb.appendJoin("join s.customer c join c.person p")
    qb.append("where 1=1")
    qb.append("and c.email = :v", null)
    qb.append("and p.name = :v", null)
    then:
    qb.getQuery() == "select s from Subscription s where 1=1"
  }

  def 'test append join - not inserted by chain, null params'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select s from Subscription s")
    qb.appendJoin("join s.customer c")
    qb.appendJoin("join c.person p")
    qb.append("where 1=1")
    qb.append("and p.name = :v", null)
    qb.append("and c.email = :v", null)
    then:
    qb.getQuery() == "select s from Subscription s where 1=1"
  }

  def 'test append join - wrong job expression'() {
    when:
    def qb = createQueryBuilder()
    qb.append("select s from Subscription s")
    qb.appendJoin("join s.customer")
    then:
    thrown IllegalArgumentException

    when:
    qb.appendJoin("s.customer c")
    then:
    thrown IllegalArgumentException

    when:
    qb.appendJoin("juin s.customer c")
    then:
    thrown IllegalArgumentException

    when:
    qb.appendJoin("join.customer c")
    then:
    thrown IllegalArgumentException
  }

  def 'test append - auto "where" removing feature'() {
    when:
    def o1 = createOrder("abc1")
    def o2 = createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.append("and o.name = :v", null)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1, o2]
  }

  def 'test append - auto "and" removing feature'() {
    when:
    def o1 = createOrder("abc1")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.append("and o.name = :v", "abc1")
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test append - auto "where" removing feature, spaces in the end'() {
    when:
    def o1 = createOrder("abc1")
    def o2 = createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where   ")
    qb.append("and o.name = :v", null)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1, o2]
  }

  def 'test append - auto "and" removing feature, spaces at the start'() {
    when:
    def o1 = createOrder("abc1")
    createOrder("cde")
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.append("   and o.name = :v", "abc1")
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, left end'() {
    when:
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 3),
        LocalDate.of(2020, 5, 5),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, right end'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 15),
        LocalDate.of(2020, 5, 17),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, intersect left end'() {
    when:
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 3),
        LocalDate.of(2020, 5, 6),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, intersect right end'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 14),
        LocalDate.of(2020, 5, 17),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, same interval'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 1),
        LocalDate.of(2020, 5, 3))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 11),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, included in search interval'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 1),
        LocalDate.of(2020, 5, 3))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 12),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 4),
        LocalDate.of(2020, 5, 11),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, includes search interval'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 1),
        LocalDate.of(2020, 5, 3))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 12),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 6),
        LocalDate.of(2020, 5, 9),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, start of search interval is null'() {
    when:
    def o1 = createOrder(
        LocalDate.of(2020, 5, 2),
        LocalDate.of(2020, 5, 6))
    def o2 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        null, LocalDate.of(2020, 5, 6),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1, o2]
  }

  def 'test appendIntervalIntersection, end of search interval is null'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 2),
        LocalDate.of(2020, 5, 6))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    def o2 = createOrder(
        LocalDate.of(2020, 5, 8),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 8), null,
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1, o2]
  }

  def 'test appendIntervalIntersection, left end is null'() {
    when:
    def o1 = createOrder(
        null,
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 3),
        LocalDate.of(2020, 5, 5),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, right end is null'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 10),
        null)
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 11),
        LocalDate.of(2020, 5, 13),
        true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, no search interval'() {
    when:
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    def o2 = createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        null, null, true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1, o2]
  }

  def 'test appendIntervalIntersection, both ends are null'() {
    when:
    def o1 = createOrder(null)
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 1),
        LocalDate.of(2020, 5, 3), true)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, left end, excluding ends'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 3),
        LocalDate.of(2020, 5, 5),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == []
  }

  def 'test appendIntervalIntersection, right end, excluding ends'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 15),
        LocalDate.of(2020, 5, 20),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == []
  }

  def 'test appendIntervalIntersection, intersect left end, excluding ends'() {
    when:
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 3),
        LocalDate.of(2020, 5, 6),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, intersect right end, excluding ends'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 14),
        LocalDate.of(2020, 5, 17),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, same interval, excluding ends'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 1),
        LocalDate.of(2020, 5, 5))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, included in search interval, excluding ends'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 1),
        LocalDate.of(2020, 5, 3))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 12),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 3),
        LocalDate.of(2020, 5, 12),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, includes search interval, excluding ends'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 1),
        LocalDate.of(2020, 5, 3))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 12),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 6),
        LocalDate.of(2020, 5, 9),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, start of search interval is null, excluding ends'() {
    when:
    def o1 = createOrder(
        LocalDate.of(2020, 5, 2),
        LocalDate.of(2020, 5, 5))
    createOrder(
        LocalDate.of(2020, 5, 6),
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        null, LocalDate.of(2020, 5, 6),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, end of search interval is null, excluding ends'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 2),
        LocalDate.of(2020, 5, 6))
    createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 8))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 7),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 8), null,
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, left end is null, excluding ends'() {
    when:
    def o1 = createOrder(
        null,
        LocalDate.of(2020, 5, 10))
    createOrder(
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 15))
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 3),
        LocalDate.of(2020, 5, 10),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  def 'test appendIntervalIntersection, right end is null, excluding ends'() {
    when:
    createOrder(
        LocalDate.of(2020, 5, 5),
        LocalDate.of(2020, 5, 10))
    def o1 = createOrder(
        LocalDate.of(2020, 5, 10),
        null)
    def qb = createQueryBuilder()
    qb.append("select o from Order o where")
    qb.appendIntervalIntersection("o.fromDate", "o.toDate",
        LocalDate.of(2020, 5, 10),
        LocalDate.of(2020, 5, 13),
        false)
    def query = qb.build(Order.class)
    then:
    query.resultList == [o1]
  }

  protected Order createOrder(String name) {
    def order = new Order()
    order.name = name
    return orderRepository.save(order)
  }

  protected Order createOrder(String name, String description) {
    def order = new Order()
    order.name = name
    order.description = description
    return orderRepository.save(order)
  }

  protected Order createOrder(LocalDate fromDate, LocalDate toDate) {
    def order = new Order()
    order.fromDate = fromDate
    order.toDate = toDate
    return orderRepository.save(order)
  }

  protected abstract QB createQueryBuilder()

}
