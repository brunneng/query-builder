package org.bitbucket.brunneng.qb.repo;

import org.bitbucket.brunneng.qb.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
