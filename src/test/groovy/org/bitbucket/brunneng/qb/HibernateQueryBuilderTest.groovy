package org.bitbucket.brunneng.qb


import org.bitbucket.brunneng.qb.dto.NamedDto

class HibernateQueryBuilderTest extends AbstractJpqlQueryBuilderTest<HibernateQueryBuilder> {

  protected HibernateQueryBuilder createQueryBuilder() {
    return new HibernateQueryBuilder(entityManager)
  }

  def 'test build native with alias to bean result transformer'() {
    when:
    def o1 = createOrder("abc1")
    def qb = createQueryBuilder()
    qb.append("select o.id as \"id\", o.name as \"name\" from orders o")
    def query = qb.buildNativeWithAliasToBeanResultTransformer(NamedDto)
    then:
    query.resultList == [new NamedDto(o1.id, o1.name)]
  }

}
