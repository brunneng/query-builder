package org.bitbucket.brunneng.qb

import org.hibernate.Session

class HibernateSpringQueryBuilderUtilsTest extends AbstractSpringQueryBuilderUtilsTest {

  protected HibernateQueryBuilder createBuilder() {
    return new HibernateQueryBuilder(entityManager.unwrap(Session.class))
  }
}
