package org.bitbucket.brunneng.qb

import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import jakarta.transaction.Transactional
import org.bitbucket.brunneng.qb.model.Order
import org.bitbucket.brunneng.qb.repo.OrderRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.test.context.jdbc.Sql
import spock.lang.Specification

@Transactional
@SpringBootTest(classes = TestApplication.class)
@Sql(statements = "delete from orders", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
abstract class AbstractSpringQueryBuilderUtilsTest extends Specification {

  @PersistenceContext
  protected EntityManager entityManager

  @Autowired
  private OrderRepository orderRepository

  def 'test paging with sort'() {
    when:
    createOrder("abc1")
    def o2 = createOrder("abc2")
    def o3 = createOrder("abc3")
    def qb = createBuilder()
    qb.append("select o from Order o")
    def page = SpringQueryBuilderUtils.loadPage(qb, PageRequest.of(0, 2,
        Sort.Direction.DESC, "name"), "id")
    then:
    page.iterator().toList() == [o3, o2]
  }

  def 'test paging with sort by same field'() {
    when:
    createOrder("abc1")
    def o2 = createOrder("abc2")
    def o3 = createOrder("abc3")
    def qb = createBuilder()
    qb.append("select o from Order o")
    def page = SpringQueryBuilderUtils.loadPage(qb, PageRequest.of(0, 2,
        Sort.Direction.DESC, "id"), "id")
    then:
    page.iterator().toList() == [o3, o2]
  }

  def 'test paging without sort'() {
    when:
    createOrder("abc1", 7)
    def o2 = createOrder("abc2", 6)
    def o3 = createOrder("abc3", 5)
    def qb = createBuilder()
    qb.append("select o from Order o")
    def page = SpringQueryBuilderUtils.loadPage(qb, PageRequest.of(0, 2), "price")
    then:
    page.iterator().toList() == [o3, o2]
  }

  def 'test paging with null unique sort field'() {
    when:
    def o1 = createOrder("abc1")
    def o2 = createOrder("abc2")
    createOrder("abc3")
    def qb = createBuilder()
    qb.append("select o from Order o")
    def page = SpringQueryBuilderUtils.loadPage(qb, PageRequest.of(0, 2), null)
    then:
    page.iterator().toList().size() == 2
  }

  private Order createOrder(String name) {
    def order = new Order()
    order.name = name
    return orderRepository.save(order)
  }

  private Order createOrder(String name, Integer price) {
    def order = new Order()
    order.name = name
    order.price = price
    return orderRepository.save(order)
  }

  abstract protected AbstractJpqlQueryBuilder createBuilder();
}
